package neu.rohanbharti.connecteddevices.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.eclipse.californium.core.CoapResource;

import neu.rohanbharti.connecteddevices.common.ConfigUtil;
import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.labs.module08.MqttClientConstants;

/**
 * GatewayDataManager Class. Sets up the Server and adds the CoapResource.
 * 
 * @author rohan_bharti
 *
 */
public class GatewayDataManager {

	private CoapServerManager coapServerManager;
	private List<CoapResource> coapResources;
	private MqttClientConnector mqttClientConnector;
	private UbidotsClientConnector ubidotsClientConnector;
	private ConfigUtil configUtil;
	private SystemPerformanceAdaptor systemPerformanceAdaptor;

	/**
	 * Constructor
	 * 
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public GatewayDataManager() throws SecurityException, IOException {
		this.coapServerManager = new CoapServerManager();
		this.coapResources = new ArrayList<CoapResource>();
		this.systemPerformanceAdaptor = new SystemPerformanceAdaptor(new SystemCpuUtilTask(), new SystemMemUtilTask());
		Thread performanceMonitoringThread = new Thread(systemPerformanceAdaptor);
		performanceMonitoringThread.start();
	}

	/**
	 * Sets up the Ubidots MQTT Client
	 * 
	 * @throws ConfigurationException
	 */
	public void setupMqttUbidotClient() throws ConfigurationException {
		this.configUtil = new ConfigUtil();

		String ubidotsUrl = this.configUtil.getStringValue(MqttClientConstants.UBIDOTS_MQTT_BROKER_URL_KEY);
		String ubidotsAuthToken = this.configUtil.getStringValue(MqttClientConstants.UBIDOTS_MQTT_AUTH_TOKEN);
		String ubidotsCertFilePath = this.configUtil
				.getStringValue(MqttClientConstants.UBIDOTS_MQTT_CERT_FILE_PATH_KEY);

		this.mqttClientConnector = new MqttClientConnector(ubidotsUrl, ubidotsAuthToken, ubidotsCertFilePath);
	}

	/**
	 * Publish to Ubidots Temp Sensor Variable every minute via Ubidots Client
	 * 
	 * @throws ConfigurationException
	 */
	public void publishToSensorVariablesByUbidotsClient(SensorData sensorData) throws ConfigurationException {

		this.ubidotsClientConnector = new UbidotsClientConnector();

		if (sensorData.getName().equals("Temperature")) {
			double tempSensorValue = sensorData.getCurrentValue();
			ubidotsClientConnector.updateTempSensorVariable(tempSensorValue);
		} 
		else if (sensorData.getName().equals("Humidity")) {
			double humiditySensorValue = sensorData.getCurrentValue();
			ubidotsClientConnector.updateHumiditySensorVariable(humiditySensorValue);
		} 
		else if (sensorData.getName().equals("Pressure")) {
			double pressureSensorValue = sensorData.getCurrentValue();
			ubidotsClientConnector.updatePressureSensorVariable(pressureSensorValue);
		} 
		else {
			return;
		}
	}

	/**
	 * Sets up the Server with its respective resources
	 */
	public void startAndSetupCoapServer() {
		if (this.coapServerManager.startSever()) {
			for (CoapResource coapResource : coapResources) {
				this.coapServerManager.registerHandler(coapResource);
			}
		}
	}

	/**
	 * Stops the Coap Server
	 */
	public void stopCoapServer() {
		this.coapServerManager.stopServer();
	}

	/**
	 * Instantiates and adds the Temperature Coap Resource
	 */
	public void addCoapResources() {
		TemperatureSensorDataHandler temperatureSensorDataHandler = new TemperatureSensorDataHandler("tempSensorData", this);
		PressureSensorDataHandler pressureSensorDataHandler = new PressureSensorDataHandler("pressureSensorData", this);
		HumiditySensorDataHandler humiditySensorDataHandler = new HumiditySensorDataHandler("humiditySensorData", this);
		this.coapResources.add(temperatureSensorDataHandler);
		this.coapResources.add(pressureSensorDataHandler);
		this.coapResources.add(humiditySensorDataHandler);
	}
	
	/**
	 * Subscribes to Ubidots Temp Actuator Variable via MQTT Client
	 */
	public void subscribeToTempActuatorVariableUbidots() throws InterruptedException {
		this.mqttClientConnector.subscribeToTopic(MqttClientConstants.ubidotsTempActuatorVariableTopic, 1);
	}

}
