package neu.rohanbharti.connecteddevices.project;

import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import neu.rohanbharti.connecteddevices.common.ActuatorDataListener;
import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;

/**
 * 
 * MqttCallback class handles the callback required for the MqttClienrConnector.
 * 
 * @author rohan_bharti
 *
 */
public class SimpleMqttCallBack implements MqttCallback {

	private static Logger logger;
	private ActuatorDataListener actuatorDataListener = new ActuatorDataListener();

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}

	/**
	 * Triggered when the connection is lost with the MqttBroker.
	 */
	@Override
	public void connectionLost(Throwable cause) {
		logger.warning("DAYUM! Connection lost from MQTT Broker!");
	}

	/**
	 * Triggered when a new message arrives. Notifies the MqttClient to do further
	 * processing with the new message received.
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		logger.info("YO! New Message arrived for the topic: " + topic + "!");
		new MqttClientConnector().messageReceived(topic, message, actuatorDataListener);
	}

	/**
	 * Triggered when the delivery is successfully completed. Also specified the
	 * token.
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		logger.info("CONGRATS! The delivery for token: " + token + " is complete!");
	}

}
