package neu.rohanbharti.connecteddevices.project;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * 
 * Class to display the performance information
 * 
 * @author rohan_bharti
 *
 */
public class SystemPerformanceAdaptor implements Runnable{

	private SystemCpuUtilTask systemCpuUtilTask;
	private SystemMemUtilTask systemMemUtilTask;

	Logger logger = Logger.getLogger("SystemPerformance");  
    FileHandler fh;  
	private static DecimalFormat decimalFormat = new DecimalFormat("#.##");

	/**
	 * Default constructor
	 */
	public SystemPerformanceAdaptor() {
		super();
	}

	/**
	 * Constructor setting SystemCpuUtilTask and SystemMemUtilTask
	 * 
	 * @param systemCpuUtilTask
	 * @param systemMemUtilTask
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public SystemPerformanceAdaptor(SystemCpuUtilTask systemCpuUtilTask, SystemMemUtilTask systemMemUtilTask) throws SecurityException, IOException {
		super();
		this.systemCpuUtilTask = systemCpuUtilTask;
		this.systemMemUtilTask = systemMemUtilTask;
		
		fh = new FileHandler("SystemPerformance.log");  
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter); 
	}

	/**
	 * Fetches the cpuUsage and memUsage from both the classes
	 */
	@Override
	public void run() {
		while(true) {
			String cpuUsage = decimalFormat.format(this.systemCpuUtilTask.getDataFromSensor()).toString();
			String memUsage = decimalFormat.format(this.systemMemUtilTask.getDataFromSensor()).toString();
	
			logger.info("CPU Usage: " + cpuUsage);
			logger.info("Memory Usage: " + memUsage);
			
			try {
				Thread.sleep(120000);
			} catch (InterruptedException e) {
				logger.severe(e.getMessage());
			}
		}
	}

}
