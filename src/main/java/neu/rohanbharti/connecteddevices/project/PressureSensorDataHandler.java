package neu.rohanbharti.connecteddevices.project;

import java.util.logging.Logger;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

import neu.rohanbharti.connecteddevices.common.DataUtil;
import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.common.SensorDataListener;
import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;

/**
 * PressureSensorDataHandler class. Serves as a CoapResource.
 * 
 * @author rohan_bharti
 *
 */
public class PressureSensorDataHandler extends CoapResource{

	private SensorDataListener sensorDataListener;
	private GatewayDataManager gatewayDataManager;
	private DataUtil dataUtil;
	
	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}
	
	/**
	 * Sets the name of the Resource
	 * 
	 * @param name
	 */
	public PressureSensorDataHandler(String name, GatewayDataManager gatewayDataManager) {
		super(name);
		getAttributes().setTitle("Pressure SensorData Handler");
		this.dataUtil = new DataUtil();
		this.gatewayDataManager = gatewayDataManager;
		registerSensorDataListener();
	}
	
	/**
	 * Sets the SensorDataListener
	 */
	public void registerSensorDataListener() {
		this.sensorDataListener = new SensorDataListener();
	}

	/**
	 * Get Request Handler
	 */
	@Override
	public void handleGET(CoapExchange exchange) {
		logger.info(this.getClass().getSimpleName() + " : GET REQUEST RECEIVED" + "\n");
		SensorData sensorData = this.sensorDataListener.getSensorData();
		String sensorDataString = this.dataUtil.toJsonFromSensorData(sensorData);
		exchange.respond(ResponseCode.VALID, sensorDataString);
	}

	/**
	 * Post Request Handler
	 */
	@Override
	public void handlePOST(CoapExchange exchange) {
		logger.info(this.getClass().getSimpleName() + " : POST REQUEST RECEIVED");
		logger.info(this.getClass().getSimpleName() + " : POST REQUEST - " + exchange.getRequestText() + "\n");
		logger.info("JSON AFTER: " + exchange.getRequestText());
		SensorData sensorData = dataUtil.toSensorDataFromJson(exchange.getRequestText());
		this.sensorDataListener.onMessage(sensorData);
		try {
			this.gatewayDataManager.publishToSensorVariablesByUbidotsClient(sensorData);
		} catch (ConfigurationException e) {
			logger.severe(e.getMessage());
		}
		String sensorDataJsonAfterProcessing = dataUtil.toJsonFromSensorData(sensorData);
		logger.info("JSON AFTER PROCESSING: " + sensorDataJsonAfterProcessing + "\n");
		exchange.respond(ResponseCode.CREATED, sensorDataJsonAfterProcessing);
	}

	/**
	 * Put Request Handler
	 */
	@Override
	public void handlePUT(CoapExchange exchange) {
		logger.info(this.getClass().getSimpleName() + " : PUT REQUEST RECEIVED");
		logger.info(this.getClass().getSimpleName() + " : PUT REQUEST - " + exchange.getRequestText());
		logger.info("JSON AFTER: " + exchange.getRequestText());
		SensorData sensorData = dataUtil.toSensorDataFromJson(exchange.getRequestText());
		this.sensorDataListener.onMessage(sensorData);
		String sensorDataJsonAfterProcessing = dataUtil.toJsonFromSensorData(sensorData);
		logger.info("JSON AFTER PROCESSING: " + sensorDataJsonAfterProcessing + "\n");
		exchange.respond(ResponseCode.CHANGED, sensorDataJsonAfterProcessing);
	}

	/**
	 * Delete Request Handler
	 */
	@Override
	public void handleDELETE(CoapExchange exchange) {
		logger.info(this.getClass().getSimpleName() + " : DELETE REQUEST RECEIVED" + "\n");
		this.sensorDataListener.onMessage(null);
		exchange.respond(ResponseCode.DELETED, "DELETE_REQUEST_SUCCESS");
	}
	
}
