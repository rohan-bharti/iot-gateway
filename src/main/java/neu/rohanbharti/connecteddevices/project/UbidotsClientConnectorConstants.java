package neu.rohanbharti.connecteddevices.project;

/**
 * Ubidots Client Connector Constants
 * 
 * @author rohan_bharti
 *
 */
public class UbidotsClientConnectorConstants {

	public static final String UBIDOTS_API_KEY = "ubidots.apiKey";
	public static final String TEMP_SENSOR_VARIABLE_ID = "5e76a2041d847217dbaea782";
	public static final String TEMP_ACTUATOR_VARIABLE_ID = "5e76a23a1d847218bcb332ec";
	public static final String HUMIDITY_SENSOR_VARIABLE_ID = "5e9273651d847267ce5193e2";
	public static final String PRESSURE_SENSOR_VARIABLE_ID = "5e9273b51d84726870718db9";
	
}
