package neu.rohanbharti.connecteddevices.project;

import java.util.logging.Logger;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;

import neu.rohanbharti.connecteddevices.common.ConfigUtil;
import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;

/**
 * 
 * CoapSeverManager class instantiates CoapServer
 * 
 * @author rohan_bharti
 *
 */
public class CoapServerManager {

	private CoapServer coapServer;
	private ConfigUtil configUtil;

	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}

	/**
	 * Constructor, sets up the CoapServer
	 */
	public CoapServerManager() {
		try {
			configUtil = new ConfigUtil();
		} catch (ConfigurationException e) {
			logger.severe("Configuration Class wasn't setup successfully!");
		}
		setupCoapServer();
	}

	/**
	 * Fetches the port using the ConfigUtil and sets up the CoapServer
	 */
	private void setupCoapServer() {
		int port = configUtil.getIntegerValue("coap.gateway.port");
		coapServer = new CoapServer(port);
	}

	/**
	 * 
	 * Starts the CoapServer
	 * 
	 * @return boolean
	 */
	public boolean startSever() {
		try {
			coapServer.start();
			logger.info("Server Started successfully!");
			return true;
		} catch (Exception e) {
			logger.severe("Server WAS NOT setup successfully!");
			return false;
		}
	}

	/**
	 * 
	 * Stops the CoapServer
	 * 
	 * @return boolean
	 */
	public boolean stopServer() {
		try {
			coapServer.stop();
			logger.info("Server Stopped successfully!");
			return true;
		} catch (Exception e) {
			logger.severe("Server WAS NOT stopped successfully!");
			return false;
		}
	}

	/**
	 * 
	 * Registers a CoapResource with the CoapServer
	 * 
	 * @return boolean
	 */
	public boolean registerHandler(CoapResource coapResource) {
		try {
			coapServer.add(coapResource);
			logger.info(coapResource.getClass().getSimpleName() + " CoapResource added successfully!");
			return true;
		} catch (Exception e) {
			logger.severe("CoapResource WAS NOT added successfully!");
			return false;
		}
	}

}
