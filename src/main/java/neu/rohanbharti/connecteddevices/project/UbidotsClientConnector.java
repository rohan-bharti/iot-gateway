package neu.rohanbharti.connecteddevices.project;

import java.util.logging.Logger;

import org.apache.commons.configuration2.ex.ConfigurationException;

import com.ubidots.*;

import neu.rohanbharti.connecteddevices.common.ConfigUtil;
import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;

/**
 * 
 * Ubidots Client Connection Class to support Ubidots API
 * 
 * @author rohan_bharti
 *
 */
public class UbidotsClientConnector {

	private ApiClient ubidotsApiClient;
	private ConfigUtil configUtil;
	private boolean isSetup = false;
	
	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}
	
	/**
	 * Constructor. Sets up the UbidotsApiClient.
	 * 
	 * @throws ConfigurationException
	 */
	public UbidotsClientConnector() throws ConfigurationException {
		configUtil = new ConfigUtil();
		setupUbidotsApiClient();
	}
	
	/**
	 * Sets new value for the Humidity Sensor Variable for myDevice
	 */
	public boolean updateHumiditySensorVariable(double humiditySensorValue) {
		if(!isSetup) {
			setupUbidotsApiClient();
		}
		
		try {
			Variable humiditySensorVariable = ubidotsApiClient.getVariable(UbidotsClientConnectorConstants.HUMIDITY_SENSOR_VARIABLE_ID);
			humiditySensorVariable.saveValue(humiditySensorValue);
			logger.info("Updated Humidity Sensor Variable with new value: " + Double.toString(humiditySensorValue));
			return true;
		} catch (Exception e) {
			logger.severe("Humidity Sensor Varibale was NOT updated successfully!");
			return false;
		}
	}
	
	/**
	 * Sets new value for the Pressure Sensor Variable for myDevice
	 */
	public boolean updatePressureSensorVariable(double pressureSensorValue) {
		if(!isSetup) {
			setupUbidotsApiClient();
		}
		
		try {
			Variable pressureSensorVariable = ubidotsApiClient.getVariable(UbidotsClientConnectorConstants.PRESSURE_SENSOR_VARIABLE_ID);
			pressureSensorVariable.saveValue(pressureSensorValue);
			logger.info("Updated Pressure Sensor Variable with new value: " + Double.toString(pressureSensorValue));
			return true;
		} catch (Exception e) {
			logger.severe("Pressure Sensor Varibale was NOT updated successfully!");
			return false;
		}
	}
	
	/**
	 * Sets new value for the Temp Sensor Variable for myDevice
	 */
	public boolean updateTempSensorVariable(double tempSensorValue) {
		if(!isSetup) {
			setupUbidotsApiClient();
		}
		
		try {
			Variable tempSensorVariable = ubidotsApiClient.getVariable(UbidotsClientConnectorConstants.TEMP_SENSOR_VARIABLE_ID);
			tempSensorVariable.saveValue(tempSensorValue);
			logger.info("Updated Temp Sensor Variable with new value: " + Double.toString(tempSensorValue));
			return true;
		} catch (Exception e) {
			logger.severe("Temp Sensor Varibale was NOT updated successfully!");
			return false;
		}
	}
	
	/**
	 * Sets new value for the Temp Actuator Variable for myDevice
	 */
	public boolean updateTempActuatorVariable(double tempActuatorValue) {
		if(!isSetup) {
			setupUbidotsApiClient();
		}
		
		try {
			Variable tempActuatorVariable = ubidotsApiClient.getVariable(UbidotsClientConnectorConstants.TEMP_ACTUATOR_VARIABLE_ID);
			tempActuatorVariable.saveValue(tempActuatorValue);
			logger.info("Updated Temp Actuator Variable with new value: " + Double.toString(tempActuatorValue));
			return true;
		} catch (Exception e) {
			logger.severe("Temp Actuator Varibale was NOT updated successfully!");
			return false;
		}
	}
	
	/**
	 * Fetches the most recent Temp Sensor Variable's value from myDevice
	 */
	public double fetchTempSensorVariableValue() {
		if(!isSetup) {
			setupUbidotsApiClient();
		}
		
		try {
			Variable tempSensorVariable = ubidotsApiClient.getVariable(UbidotsClientConnectorConstants.TEMP_SENSOR_VARIABLE_ID);
			Value[] tempSensorValues = tempSensorVariable.getValues();
			double mostRecentTempSensorValue = tempSensorValues[tempSensorValues.length - 1].getValue();
			logger.info("Most Recent Temp Sensor Variable's value: " + Double.toString(mostRecentTempSensorValue));
			return mostRecentTempSensorValue;
		} catch (Exception e) {
			logger.severe("Temp Sensor Varibale's value was NOT fetched successfully!");
			return -1;
		}
	}
	
	/**
	 * Fetches the most recent Temp Actuator Variable's value from myDevice
	 */
	public double fetchTempActuatorVariableValue() {
		if(!isSetup) {
			setupUbidotsApiClient();
		}
		
		try {
			Variable tempActuatorVariable = ubidotsApiClient.getVariable(UbidotsClientConnectorConstants.TEMP_ACTUATOR_VARIABLE_ID);
			Value[] tempActuatorValues = tempActuatorVariable.getValues();
			double mostRecentTempActuatorValue = tempActuatorValues[tempActuatorValues.length - 1].getValue();
			logger.info("Most Recent Temp Actuator Variable's value: " + Double.toString(mostRecentTempActuatorValue));
			return mostRecentTempActuatorValue;
		} catch (Exception e) {
			logger.severe("Temp Actuator Varibale's value was NOT fetched successfully!");
			return -1;
		}
	}

	/**
	 * Helper method to set up the Ubidots API.
	 */
	private void setupUbidotsApiClient() {
		try {
			String apiKey = configUtil.getStringValue(UbidotsClientConnectorConstants.UBIDOTS_API_KEY);
			ubidotsApiClient = new ApiClient(apiKey);
			logger.info("Ubidots API Client setup successfully!");
			isSetup = true;
		} catch (Exception e) {
			logger.severe("Ubidots API Client was NOT setup successfully!");
		}
	}
	
}
