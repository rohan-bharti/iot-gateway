package neu.rohanbharti.connecteddevices.project;

import java.io.IOException;

import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 * GatewayHandlerApp class. Instantiates the app.
 * 
 * @author rohan_bharti
 *
 */
public class GatewayHandlerApp {

	private GatewayDataManager gatewayDataManager;

	/**
	 * Constructor
	 * 
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public GatewayHandlerApp() throws SecurityException, IOException {
		this.gatewayDataManager = new GatewayDataManager();
	}

	/**
	 * Starts the Coap Server and listens to incoming CoAP requests based on which it pushes to the
	 * Ubidots Sensor Variables via Ubidots Client (HTTP)
	 */
	public void startCoapServer() {
		this.gatewayDataManager.addCoapResources();
		this.gatewayDataManager.startAndSetupCoapServer();
	}
	
	/**
	 * Starts the Mqtt Ubidots Client. Listens for Actuation and pushes them to the Device App via MQTT
	 * 
	 * @throws ConfigurationException 
	 * @throws InterruptedException 
	 */
	public void startMqttProcess() throws ConfigurationException, InterruptedException {
		this.gatewayDataManager.setupMqttUbidotClient();
		this.gatewayDataManager.subscribeToTempActuatorVariableUbidots();
	}

	/**
	 * Instantiates GatewayHandlerApp and calls the startApplication method
	 * 
	 * @throws InterruptedException 
	 * @throws ConfigurationException 
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public static void main(String[] args) throws ConfigurationException, InterruptedException, SecurityException, IOException {
		GatewayHandlerApp gatewayHandlerApp = new GatewayHandlerApp();
		gatewayHandlerApp.startCoapServer();
		gatewayHandlerApp.startMqttProcess();
	}

}
