package neu.rohanbharti.connecteddevices.labs.module08;

import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 * GatewayHandlerApp class. Instantiates the app.
 * 
 * @author rohan_bharti
 *
 */
public class GatewayHandlerApp {

	private GatewayDataManager gatewayDataManager;

	/**
	 * Constructor
	 * 
	 * @throws ConfigurationException 
	 */
	public GatewayHandlerApp() throws ConfigurationException {
		this.gatewayDataManager = new GatewayDataManager();
	}

	/**
	 * Starts the application
	 * 
	 * @throws InterruptedException 
	 * @throws ConfigurationException 
	 */
	public void startApplication() throws InterruptedException, ConfigurationException {
		this.gatewayDataManager.setupMqttUbidotClient();
//		this.gatewayDataManager.publishTempSensorByMQTTClient();
		this.gatewayDataManager.publishTempSensorByUbidotsClient();
		this.gatewayDataManager.subscribeToTempActuatorVariableUbidots();
	}

	/**
	 * Instantiates GatewayHandlerApp and calls the startApplication method
	 * 
	 * @throws InterruptedException 
	 * @throws ConfigurationException 
	 */
	public static void main(String[] args) throws InterruptedException, ConfigurationException {
		GatewayHandlerApp gatewayHandlerApp = new GatewayHandlerApp();
		gatewayHandlerApp.startApplication();
	}

}
