package neu.rohanbharti.connecteddevices.labs.module08;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import neu.rohanbharti.connecteddevices.common.SensorData;

/**
 * 
 * Temperature Sensor Emulator Class
 * 
 * @author rohan_bharti
 *
 */
public class TempSensorEmulator {

	private final double minTempValue = 5.0;
	private final double maxTempValue = 35.0;
	
	private final Random random = new Random();
	
	/**
	 * Generates a random temperature value between 0 and 30.
	 */
	public double generateRandomTemperatureValue() {
		double newTempValue = minTempValue + random.nextDouble() * (maxTempValue - minTempValue);
		return newTempValue;
	}
	
}
