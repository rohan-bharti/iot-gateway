package neu.rohanbharti.connecteddevices.labs.module08;

import java.util.UUID;

/**
 * 
 * Constants for the MqttClientConnector.
 * 
 * @author rohan_bharti
 *
 */
public class MqttClientConstants {

	public static final String gatewayClientId = UUID.randomUUID().toString();
	public static final String brokerAddress = "tcp://mqtt.eclipse.org:1883";
	public static final String sensorDataTopic = "topic/rohan/test";
	public static final String actuatorDataTopic = "topic/rohan/actuatorData";
	public static final String ubidotsTempActuatorVariableTopic = "/v1.6/devices/mydevice/temp-actuator-variable/lv";
	public static final String ubidotsTempSensorVariableTopic = "/v1.6/devices/mydevice/temp-sensor-variable";
	public static final boolean mqttMessageRetainPolicy = true;
	
	public static final String UBIDOTS_MQTT_BROKER_URL_KEY = "ubidots.brokerUrl";
	public static final String UBIDOTS_MQTT_CERT_FILE_PATH_KEY = "ubidots.certFilePath";
	public static final String UBIDOTS_MQTT_AUTH_TOKEN = "ubidots.authToken";

}
