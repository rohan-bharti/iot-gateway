package neu.rohanbharti.connecteddevices.labs.module08;

/**
 * Ubidots Client Connector Constants
 * 
 * @author rohan_bharti
 *
 */
public class UbidotsClientConnectorConstants {

	public static final String UBIDOTS_API_KEY = "ubidots.apiKey";
	public static final String TEMP_SENSOR_VARIABLE_ID = "5e76a2041d847217dbaea782";
	public static final String TEMP_ACTUATOR_VARIABLE_ID = "5e76a23a1d847218bcb332ec";
	
}
