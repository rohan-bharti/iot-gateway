package neu.rohanbharti.connecteddevices.labs.module08;

import java.util.logging.Logger;

import javax.net.ssl.SSLSocketFactory;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.labbenchstudios.iot.common.CertManagementUtil;

import neu.rohanbharti.connecteddevices.common.ActuatorData;
import neu.rohanbharti.connecteddevices.common.ActuatorDataListener;
import neu.rohanbharti.connecteddevices.common.DataUtil;
import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.common.SensorDataListener;
import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;

/**
 * 
 * MqttClientConnector class, handles processing of message received via
 * MqttBroker. Also serves as the MqttClient for Ubidots.
 * 
 * @author rohan_bharti
 *
 */
public class MqttClientConnector {

	private MqttClient mqttClient;
	private SensorDataListener sensorDataListener;
	private ActuatorDataListener actuatorDataListener;
	private DataUtil dataUtil;
	private CertManagementUtil certManagementUtil;
	private boolean isConnected = false;
	private boolean isTlsEnabled = false;

	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}

	/**
	 * Default Constructor
	 */
	public MqttClientConnector() {
		dataUtil = new DataUtil();
	}

	/**
	 * MqttClient Connector Constructor for setting up client for Ubidots
	 */
	public MqttClientConnector(String brokerUrl, String authToken, String tlsCertFilePath) {
		certManagementUtil = CertManagementUtil.getInstance();
		dataUtil = new DataUtil();
		this.isTlsEnabled = true;
		setupMqttClientForUbidots(brokerUrl, authToken, tlsCertFilePath);
	}

	/**
	 * MqttClient Connector Client Setup for Ubidots
	 */
	private void setupMqttClientForUbidots(String brokerUrl, String authToken, String tlsCertFilePath) {
		if (isTlsEnabled) {
			try {
				mqttClient = new MqttClient(brokerUrl, MqttClientConstants.gatewayClientId);
				MqttConnectOptions conOpt = new MqttConnectOptions();
				conOpt.setCleanSession(true);
				conOpt.setUserName(authToken);
				SSLSocketFactory socketFactory = certManagementUtil.loadCertificate(tlsCertFilePath);
				conOpt.setSocketFactory(socketFactory);
				if (!mqttClient.isConnected()) {
					mqttClient.connect(conOpt);
					logger.info("Connected to the Ubidots MQTT Broker successfully");
					this.isConnected = true;
					registerActuatorDataListener();
				}
			} catch (MqttException e) {
				logger.severe("Ubidots MQTT Client wasn't set up successfully!");
			}
		}
	}

	/**
	 * Sets up the MqttClient
	 */
	private void setupMqttClient() {
		if (!isTlsEnabled) {
			try {
				mqttClient = new MqttClient(MqttClientConstants.brokerAddress, MqttClientConstants.gatewayClientId);
				MqttConnectOptions conOpt = new MqttConnectOptions();
				conOpt.setCleanSession(false);
				conOpt.setKeepAliveInterval(10);
				mqttClient.connect(conOpt);
				logger.info("Connected to the MQTT Broker successfully");
				this.isConnected = true;
			} catch (MqttException e) {
				logger.severe("MQTT Client wasn't set up successfully!");
			}
		}
	}

	/**
	 * Published actuatorData on the actuatorData topic channel
	 * 
	 * @param actuatorData
	 * @param QOS
	 * @return
	 */
	public boolean publishActuatorCommand(ActuatorData actuatorData, int QOS) {
		if (!this.isConnected) {
			setupMqttClient();
		}

		String actuatorDataString = null;
		actuatorDataString = dataUtil.toJsonFromActuatorData(actuatorData);
		logger.info("JSON BEFORE: " + actuatorDataString);

		MqttMessage message = new MqttMessage(actuatorDataString.getBytes());
		message.setQos(QOS);
		message.setRetained(MqttClientConstants.mqttMessageRetainPolicy);

		try {
			mqttClient.publish(MqttClientConstants.actuatorDataTopic, message);
			logger.info("ActuatorData published successfully to the Device App");
			return true;
		} catch (MqttPersistenceException e) {
			logger.severe(e.getMessage());
			return false;
		} catch (MqttException e) {
			logger.severe(e.getMessage());
			return false;
		}
	}

	/**
	 * Publishes sensorData on the SensorData topic channel.
	 * 
	 * @param sensorData
	 * @param QOS
	 * @return
	 */
	public boolean publishSensorData(SensorData sensorData, int QOS) {
		if (!isConnected) {
			setupMqttClient();
		}

		String sensorDataString = null;
		sensorDataString = dataUtil.toJsonFromSensorData(sensorData);
		logger.info("JSON BEFORE: " + sensorDataString);

		MqttMessage message = new MqttMessage(sensorDataString.getBytes());
		message.setQos(QOS);
		message.setRetained(MqttClientConstants.mqttMessageRetainPolicy);

		try {
			mqttClient.publish(MqttClientConstants.sensorDataTopic, message);
			logger.info("SensorData published successfully");
			return true;
		} catch (MqttPersistenceException e) {
			logger.severe(e.getMessage());
			return false;
		} catch (MqttException e) {
			logger.severe(e.getMessage());
			return false;
		}
	}

	/**
	 * Publishes sensorData on the Ubidots Temp Sensor Variable topic channel.
	 * 
	 * @param sensorData
	 * @param QOS
	 * @return
	 */
	public boolean publishUbidotsTempSensorData(SensorData sensorData, int QOS) {
		if (!isConnected) {
			logger.severe("Ubidots MQTT Client connection NOT setup successfully!");
			return false;
		}

		String ubidotsTempSensorDataString = null;
		ubidotsTempSensorDataString = dataUtil.toUbidotJsonFromSensorData(sensorData);
		logger.info("Ubidots JSON BEFORE: " + ubidotsTempSensorDataString);

		MqttMessage message = new MqttMessage(ubidotsTempSensorDataString.getBytes());
		message.setQos(QOS);
		message.setRetained(MqttClientConstants.mqttMessageRetainPolicy);

		try {
			mqttClient.publish(MqttClientConstants.ubidotsTempSensorVariableTopic, message);
			logger.info("SensorData published successfully to Ubidots TempSensor Variable");
			return true;
		} catch (MqttPersistenceException e) {
			logger.severe(e.getMessage());
			return false;
		} catch (MqttException e) {
			logger.severe(e.getMessage());
			return false;
		}
	}

	/**
	 * Subscribes and listens to the topic channel supplied.
	 * 
	 * @throws InterruptedException
	 */
	public void subscribeToTopic(String topic, int qos) throws InterruptedException {
		if (!isConnected) {
			setupMqttClient();
		}
		mqttClient.setCallback(new SimpleMqttCallBack());

		try {
			mqttClient.subscribe(topic, qos);
			logger.info("Subscribed successfully to: " + topic + "!");
		} catch (MqttSecurityException e1) {
			logger.severe("MQTT Connection Secuity Error while subscribing to the Topic: " + topic + "!");
		} catch (MqttException e) {
			logger.severe("MQTT subscribtion to the Topic: " + topic + " was UNSUCCESSFUL!");
		}
	}

	/**
	 * Helper method to unsubscribe and disconnect from the MQTT Broker
	 * successfully!
	 * 
	 * @param topicFilter
	 */
	public void closeConnection(String topicFilter) {
		try {
			this.mqttClient.unsubscribe(topicFilter);
			this.mqttClient.disconnect();
			logger.info("Unsubscribed from the topic: " + topicFilter + " and closed the connection");
		} catch (MqttException e) {
			logger.severe("Connection wasn't closed successfully");
		}
	}

	/**
	 * Analyzes the topic and message received and does further processing.
	 * 
	 * @param topic
	 * @param message
	 * @throws JsonProcessingException
	 */
	public void messageReceived(String topic, MqttMessage message, ActuatorDataListener actuatorDataListener) {
		if (topic.equals(MqttClientConstants.sensorDataTopic)) {
			processSensorData(message);
		} else if (topic.equals(MqttClientConstants.actuatorDataTopic)) {
			processActuatorData(message);
		} else if (topic.equals(MqttClientConstants.ubidotsTempActuatorVariableTopic)) {
			processUbidotsTempActuatorData(message, actuatorDataListener);
		} else {
			logger.info("Message" + message + " received for topic: " + topic + "!");
		}
	}

	/**
	 * Converts the Message received from the Ubidots and registers the ActuatorData
	 * Listener
	 * 
	 * @param message
	 */
	public ActuatorData processUbidotsTempActuatorData(MqttMessage message, ActuatorDataListener actuatorDataListener) {
		String latestTempActuatorValue = message.toString();
		logger.info("Ubidots Temp Actuator Value: " + latestTempActuatorValue);
		ActuatorData actuatorData = dataUtil.processUbidotsActuatorData(latestTempActuatorValue);
		String actuatorDataJsonAfterProcessing = dataUtil.toJsonFromActuatorData(actuatorData);
		logger.info("JSON AFTER PROCESSING: " + actuatorDataJsonAfterProcessing + "\n");
		registerActuatorDataListener();
		actuatorDataListener.onMessage(actuatorData);
		return actuatorData;
	}

	/**
	 * Converts the SensorData message to SensorData object and registers the
	 * SensorDataListener.
	 * 
	 * @param message
	 * @return
	 * @throws JsonProcessingException
	 */
	public SensorData processSensorData(MqttMessage message) {
		String sensorDataJsonString = message.toString();
		logger.info("JSON AFTER: " + sensorDataJsonString);
		SensorData sensorData = dataUtil.toSensorDataFromJson(sensorDataJsonString);
		String sensorDataJsonAfterProcessing = dataUtil.toJsonFromSensorData(sensorData);
		logger.info("JSON AFTER PROCESSING: " + sensorDataJsonAfterProcessing + "\n");
		registerSensorDataListener();
		this.sensorDataListener.onMessage(sensorData);
		return sensorData;
	}

	/**
	 * Registers the SensorDataListener.
	 */
	private void registerSensorDataListener() {
		this.sensorDataListener = new SensorDataListener();
	}

	/**
	 * Converts the ActuatorData message to ActuatorData object and registers the
	 * ActuatorDataListener.
	 * 
	 * @param message
	 * @return
	 * @throws JsonProcessingException
	 */
	public ActuatorData processActuatorData(MqttMessage message) {
		String actuatorDataString = message.toString();
		logger.info("JSON AFTER: " + actuatorDataString);
		ActuatorData actuatorData = dataUtil.toActuatorDataFromJson(actuatorDataString);
		String actuatorDataJsonAfterProcessing = dataUtil.toJsonFromActuatorData(actuatorData);
		logger.info("JSON AFTER PROCESSING: " + actuatorDataJsonAfterProcessing + "\n");
		actuatorDataListener.onMessage(actuatorData);
		return actuatorData;
	}

	/**
	 * Registers the ActuatorDataListener.
	 */
	private void registerActuatorDataListener() {
		if(this.actuatorDataListener == null)
			this.actuatorDataListener = new ActuatorDataListener();
	}

	/**
	 * Setter method to enable TLS Certificate functionality
	 * 
	 * @param isTlsEnabled
	 */
	public void setTlsEnabled(boolean isTlsEnabled) {
		this.isTlsEnabled = isTlsEnabled;
	}

}
