package neu.rohanbharti.connecteddevices.labs.module08;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.ex.ConfigurationException;

import neu.rohanbharti.connecteddevices.common.ConfigUtil;
import neu.rohanbharti.connecteddevices.common.SensorData;

/**
 * GatewayDataManager Class. Sets up the Coap Server, adds the CoapResource, the MQTTClient and the UbidotsAPI Client Connector.
 * 
 * @author rohan_bharti
 *
 */
public class GatewayDataManager {

	private MqttClientConnector mqttClientConnector;
	private UbidotsClientConnector ubidotsClientConnector;
	private ConfigUtil configUtil;

	/**
	 * Sets up the Ubidots MQTT Client
	 * 
	 * @throws ConfigurationException 
	 */
	public void setupMqttUbidotClient() throws ConfigurationException {
		this.configUtil = new ConfigUtil();
		
		String ubidotsUrl = this.configUtil.getStringValue(MqttClientConstants.UBIDOTS_MQTT_BROKER_URL_KEY);
		String ubidotsAuthToken = this.configUtil.getStringValue(MqttClientConstants.UBIDOTS_MQTT_AUTH_TOKEN);
		String ubidotsCertFilePath = this.configUtil.getStringValue(MqttClientConstants.UBIDOTS_MQTT_CERT_FILE_PATH_KEY);
		
		this.mqttClientConnector = new MqttClientConnector(ubidotsUrl, ubidotsAuthToken, ubidotsCertFilePath);
	}
	
	/**
	 * Publish to Ubidots Temp Sensor Variable every minute via Ubidots Client
	 * 
	 * @throws ConfigurationException 
	 */
	public void publishTempSensorByUbidotsClient() throws ConfigurationException {
		
		this.ubidotsClientConnector = new UbidotsClientConnector();
		
		ScheduledExecutorService eventScheduler = Executors.newSingleThreadScheduledExecutor();
		TempSensorEmulator tempSensorEmulator = new TempSensorEmulator();
		
		Runnable publishTempValues = new Runnable() {
			public void run() {
				double tempSensorValue = tempSensorEmulator.generateRandomTemperatureValue();
				ubidotsClientConnector.updateTempSensorVariable(tempSensorValue);
			}
		};
		
		eventScheduler.scheduleWithFixedDelay(publishTempValues, 1, 60, TimeUnit.SECONDS);
	}
	
	/**
	 * Publish to Ubidots Temp Sensor Variable every minute via MQTT Client
	 */
	public void publishTempSensorByMQTTClient() {
		ScheduledExecutorService eventScheduler = Executors.newSingleThreadScheduledExecutor();
		TempSensorEmulator tempSensorEmulator = new TempSensorEmulator();
		SensorData sensorData = new SensorData();
		
		Runnable publishTempValues = new Runnable() {
			public void run() {
				double tempSensorValue = tempSensorEmulator.generateRandomTemperatureValue();
				sensorData.addValue((float)tempSensorValue, "Temperature");
				mqttClientConnector.publishUbidotsTempSensorData(sensorData, 1);
			}
		};
		
		eventScheduler.scheduleWithFixedDelay(publishTempValues, 1, 60, TimeUnit.SECONDS);
	}
	
	/**
	 * Subscribes to Ubidots Temp Actuator Variable via MQTT Client
	 */
	public void subscribeToTempActuatorVariableUbidots() throws InterruptedException {
		this.mqttClientConnector.subscribeToTopic(MqttClientConstants.ubidotsTempActuatorVariableTopic, 1);
	}

}
