package neu.rohanbharti.connecteddevices.labs.module06;

import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

import com.fasterxml.jackson.core.JsonProcessingException;

import neu.rohanbharti.connecteddevices.common.ActuatorData;
import neu.rohanbharti.connecteddevices.common.ActuatorDataListener;
import neu.rohanbharti.connecteddevices.common.DataUtil;
import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.common.SensorDataListener;
import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;

/**
 * 
 * MqttClientConnector class, handles processing of message received via
 * MqttBroker.
 * 
 * @author rohan_bharti
 *
 */
public class MqttClientConnector {

	private MqttClient mqttClient;

	private SensorDataListener sensorDataListener;
	private ActuatorDataListener actuatorDataListener;
	private DataUtil dataUtil;
	private boolean isConnected;

	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}

	/**
	 * Default Constructor
	 */
	public MqttClientConnector() {
		isConnected = false;
		dataUtil = new DataUtil();
	}

	/**
	 * Sets up the MqttClient
	 */
	private void setupMqttClient() {
		try {
			mqttClient = new MqttClient(MqttClientConstants.brokerAddress, MqttClientConstants.gatewayClientId);
			MqttConnectOptions conOpt = new MqttConnectOptions();
			conOpt.setCleanSession(false);
			conOpt.setKeepAliveInterval(10);
			mqttClient.connect(conOpt);
			logger.info("Connected to the MQTT Broker successfully");
		} catch (MqttException e) {
			logger.severe("MQTT Client wasn't set up successfully!");
		}
	}

	/**
	 * Published actuatorData on the actuatorData topic channel
	 * 
	 * @param actuatorData
	 * @param QOS
	 * @return
	 */
	public boolean publishActuatorCommand(ActuatorData actuatorData, int QOS) {
		if (!isConnected) {
			setupMqttClient();
		}

		String actuatorDataString = null;
		actuatorDataString = dataUtil.toJsonFromActuatorData(actuatorData);
		logger.info("JSON BEFORE: " + actuatorDataString);

		MqttMessage message = new MqttMessage(actuatorDataString.getBytes());
		message.setQos(QOS);
		message.setRetained(MqttClientConstants.mqttMessageRetainPolicy);

		try {
			mqttClient.publish(MqttClientConstants.actuatorDataTopic, message);
			logger.info("ActuatorData published successfully");
			return true;
		} catch (MqttPersistenceException e) {
			logger.severe(e.getMessage());
			return false;
		} catch (MqttException e) {
			logger.severe(e.getMessage());
			return false;
		}
	}

	/**
	 * Publishes sensorData on the SensorData topic channel.
	 * 
	 * @param sensorData
	 * @param QOS
	 * @return
	 */
	public boolean publishSensorData(SensorData sensorData, int QOS) {
		if (!isConnected) {
			setupMqttClient();
		}

		String sensorDataString = null;
		sensorDataString = dataUtil.toJsonFromSensorData(sensorData);
		logger.info("JSON BEFORE: " + sensorDataString);

		MqttMessage message = new MqttMessage(sensorDataString.getBytes());
		message.setQos(QOS);
		message.setRetained(MqttClientConstants.mqttMessageRetainPolicy);

		try {
			mqttClient.publish(MqttClientConstants.sensorDataTopic, message);
			logger.info("SensorData published successfully");
			return true;
		} catch (MqttPersistenceException e) {
			logger.severe(e.getMessage());
			return false;
		} catch (MqttException e) {
			logger.severe(e.getMessage());
			return false;
		}
	}

	/**
	 * Subscribes and listens to the actuatorData topic channel.
	 * @throws InterruptedException 
	 */
	public void subscribeToActuatorCommands(int qos) throws InterruptedException {
		if (!isConnected) {
			setupMqttClient();
		}
		mqttClient.setCallback(new SimpleMqttCallBack());

		try {
			mqttClient.subscribe(MqttClientConstants.actuatorDataTopic, qos);
			logger.info("Subscribed successfully to: " + MqttClientConstants.actuatorDataTopic + "!");
			Thread.sleep(65000);
			closeConnection(MqttClientConstants.actuatorDataTopic);
		} catch (MqttSecurityException e1) {
			logger.severe("MQTT Connection Secuity Error while subscribing to the Topic!");
		} catch (MqttException e) {
			logger.severe("MQTT subscribtion to the Topic was UNSUCCESSFUL!");
		}
	}

	/**
	 * Subscribes and listens to the sensorData topic channel.
	 * @throws InterruptedException 
	 */
	public void subscribeToSensorCommands(int qos) throws InterruptedException {
		if (!isConnected) {
			setupMqttClient();
		}
		mqttClient.setCallback(new SimpleMqttCallBack());
		try {
			mqttClient.subscribe(MqttClientConstants.sensorDataTopic, qos);
			logger.info("Subscribed successfully to: " + MqttClientConstants.sensorDataTopic + "!");
			Thread.sleep(65000);
			closeConnection(MqttClientConstants.sensorDataTopic);
		} catch (MqttSecurityException e1) {
			logger.severe("MQTT Connection Secuity Error while subscribing to the Topic!");
		} catch (MqttException e) {
			logger.severe("MQTT subscribtion to the Topic was UNSUCCESSFUL!");
		}
	}

	/**
	 * Helper method to unsubscribe and disconnect from the MQTT Broker successfully!
	 * 
	 * @param topicFilter
	 */
	private void closeConnection(String topicFilter) {
		try {
			this.mqttClient.unsubscribe(topicFilter);
			this.mqttClient.disconnect();
			logger.info("Unsubscribed from the topic: " + topicFilter + " and closed the connection");
		} catch (MqttException e) {
			logger.severe("Connection wasn't closed successfully");
		}
	}
	
	/**
	 * Analyzes the topic and message received and does further processing.
	 * 
	 * @param topic
	 * @param message
	 * @throws JsonProcessingException
	 */
	public void messageReceived(String topic, MqttMessage message) {
		if (topic.equals(MqttClientConstants.sensorDataTopic)) {
			processSensorData(message);
		} else if (topic.equals(MqttClientConstants.actuatorDataTopic)) {
			processActuatorData(message);
		} else {
			logger.info("Message" + message + " received for topic: " + topic + "!");
		}
	}

	/**
	 * Converts the SensorData message to SensorData object and registers the
	 * SensorDataListener.
	 * 
	 * @param message
	 * @return
	 * @throws JsonProcessingException
	 */
	public SensorData processSensorData(MqttMessage message) {
		String sensorDataJsonString = message.toString();
		logger.info("JSON AFTER: " + sensorDataJsonString);
		SensorData sensorData = dataUtil.toSensorDataFromJson(sensorDataJsonString);
		String sensorDataJsonAfterProcessing = dataUtil.toJsonFromSensorData(sensorData);
		logger.info("JSON AFTER PROCESSING: " + sensorDataJsonAfterProcessing + "\n");
		registerSensorDataListener();
		this.sensorDataListener.onMessage(sensorData);
		return sensorData;
	}

	/**
	 * Registers the SensorDataListener.
	 */
	private void registerSensorDataListener() {
		this.sensorDataListener = new SensorDataListener();
	}

	/**
	 * Converts the ActuatorData message to ActuatorData object and registers the
	 * ActuatorDataListener.
	 * 
	 * @param message
	 * @return
	 * @throws JsonProcessingException
	 */
	public ActuatorData processActuatorData(MqttMessage message) {
		String actuatorDataString = message.toString();
		logger.info("JSON AFTER: " + actuatorDataString);
		ActuatorData actuatorData = dataUtil.toActuatorDataFromJson(actuatorDataString);
		String actuatorDataJsonAfterProcessing = dataUtil.toJsonFromActuatorData(actuatorData);
		logger.info("JSON AFTER PROCESSING: " + actuatorDataJsonAfterProcessing + "\n");
		registerActuatorDataListener();
		actuatorDataListener.onMessage(actuatorData);
		return actuatorData;
	}

	/**
	 * Registers the ActuatorDataListener.
	 */
	private void registerActuatorDataListener() {
		this.actuatorDataListener = new ActuatorDataListener();
	}

}
