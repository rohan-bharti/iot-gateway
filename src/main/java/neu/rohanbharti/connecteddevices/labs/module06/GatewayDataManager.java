package neu.rohanbharti.connecteddevices.labs.module06;

/**
 * 
 * Gateway Manager which initiates the MQTT Client
 * 
 * @author rohan_bharti
 *
 */
public class GatewayDataManager {

	private MqttClientConnector mqttClientConnector;

	/**
	 * Constructor
	 */
	public GatewayDataManager() {
		this.mqttClientConnector = new MqttClientConnector();
	}

	/**
	 * Starts Listening for a new message from the MQTT Broker
	 * @throws InterruptedException 
	 */
	public void startListeningToMqttBroker() throws InterruptedException {
		this.mqttClientConnector.subscribeToSensorCommands(1);
	}

	/**
	 * Executes the application
	 * 
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void demo() throws InterruptedException {
		GatewayDataManager gatewayDataManager = new GatewayDataManager();
		gatewayDataManager.startListeningToMqttBroker();
	}

}
