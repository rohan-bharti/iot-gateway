package neu.rohanbharti.connecteddevices.labs.module06;

import java.util.UUID;

/**
 * 
 * Constants for the MqttClientConnector.
 * 
 * @author rohan_bharti
 *
 */
public class MqttClientConstants {

	public static final String gatewayClientId = UUID.randomUUID().toString();
	public static final String brokerAddress = "tcp://mqtt.eclipse.org:1883";
	public static final String sensorDataTopic = "topic/rohan/test";
	public static final String actuatorDataTopic = "topic/actuatorTest";
	public static final boolean mqttMessageRetainPolicy = true;

}
