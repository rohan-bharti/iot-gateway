package neu.rohanbharti.connecteddevices.labs.module06;

/**
 * 
 * Main Application class
 * 
 * @author rohan_bharti
 *
 */
public class GatewayHandlerApp {

	/**
	 * Runs the application, subscribes to the SensorData channel to listen for any
	 * update on that topic.
	 * 
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		GatewayDataManager.demo();
	}

}
