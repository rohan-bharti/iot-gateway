package neu.rohanbharti.connecteddevices.labs.module07;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.californium.core.CoapResource;

/**
 * GatewayDataManager Class. Sets up the Server and adds the CoapResource.
 * 
 * @author rohan_bharti
 *
 */
public class GatewayDataManager {

	private CoapServerManager coapServerManager;
	private List<CoapResource> coapResources;

	/**
	 * Constructor
	 */
	public GatewayDataManager() {
		this.coapServerManager = new CoapServerManager();
		this.coapResources = new ArrayList<CoapResource>();
	}

	/**
	 * Sets up the Server with its respective resources
	 */
	public void startAndSetupServer() {
		if (this.coapServerManager.startSever()) {
			for (CoapResource coapResource : coapResources) {
				this.coapServerManager.registerHandler(coapResource);
			}
		}
	}

	/**
	 * Stops the Coap Server
	 */
	public void stopServer() {
		this.coapServerManager.stopServer();
	}

	/**
	 * Instantiates and adds the Temperature Coap Resource
	 */
	public void addTemperatureCoapResource() {
		TemperatureSensorDataHandler temperatureSensorDataHandler = new TemperatureSensorDataHandler("tempSensorData");
		this.coapResources.add(temperatureSensorDataHandler);
	}

}
