package neu.rohanbharti.connecteddevices.labs.module07;

/**
 * GatewayHandlerApp class. Instantiates the app.
 * 
 * @author rohan_bharti
 *
 */
public class GatewayHandlerApp {

	private GatewayDataManager gatewayDataManager;

	/**
	 * Constructor
	 */
	public GatewayHandlerApp() {
		this.gatewayDataManager = new GatewayDataManager();
	}

	/**
	 * Starts the application
	 */
	public void startApplication() {
		this.gatewayDataManager.addTemperatureCoapResource();
		this.gatewayDataManager.startAndSetupServer();
	}

	/**
	 * Instantiates GatewayHandlerApp and calls the startApplication method
	 */
	public static void main(String[] args) {
		GatewayHandlerApp gatewayHandlerApp = new GatewayHandlerApp();
		gatewayHandlerApp.startApplication();
	}

}
