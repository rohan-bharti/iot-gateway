package neu.rohanbharti.connecteddevices.labs.module05;

import org.apache.commons.configuration2.ex.ConfigurationException;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 
 * Driver class to start the Gateway App
 * 
 * @author rohan_bharti
 *
 */
public class GatewayHandlerApp {

	/**
	 * Kicks off the application
	 * 
	 * @param args
	 * @throws InterruptedException
	 * @throws ConfigurationException 
	 * @throws JsonProcessingException 
	 */
	public static void main(String[] args) throws InterruptedException, ConfigurationException, JsonProcessingException {
		
		GatewayDataManager gatewayDataManager = new GatewayDataManager();
		gatewayDataManager.checkForSensorDataUpdate();
		
	}
}
