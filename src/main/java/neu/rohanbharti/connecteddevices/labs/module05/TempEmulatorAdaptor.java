package neu.rohanbharti.connecteddevices.labs.module05;

/**
 * 
 * Adaptor class for TempSensorEmulator. Calls the scheduling function for Temperature Values generation and email
 * service.
 * 
 * @author rohan_bharti
 *
 */
public class TempEmulatorAdaptor {

	private static TempSensorEmulator tempSensorEmulator;
	
	/**
	 * Runs the scheduling function of the TempSensorEmulator class.
	 */
	public static void demo() {
		tempSensorEmulator = new TempSensorEmulator();
		tempSensorEmulator.scheduleTemperatureMonitoring();
	}
}
