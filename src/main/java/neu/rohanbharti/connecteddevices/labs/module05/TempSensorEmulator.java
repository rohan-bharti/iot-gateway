package neu.rohanbharti.connecteddevices.labs.module05;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import neu.rohanbharti.connecteddevices.common.SensorData;

/**
 * 
 * Temperature Sensor Emulator Class
 * 
 * @author rohan_bharti
 *
 */
public class TempSensorEmulator {

	private final float alertingDifference = 10.0f;
	private final float minTempValue = 0.0f;
	private final float maxTempValue = 30.0f;
	private boolean emailSent = false;
	
	private final Random random = new Random();
	
	private ScheduledExecutorService eventScheduler;
	
	private static Logger logger;
	
	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(TempSensorEmulator.class.getName());
	}
	
	private SmtpClientConnector smtpClientConnector;
	private SensorData sensorData;
	
	/**
	 * Default constructor that initializes the SmtpClientConnector and the SensorData object.
	 */
	public TempSensorEmulator() {
		super();
		this.smtpClientConnector = new SmtpClientConnector();
		this.sensorData = new SensorData();
	}
	
	/**
	 * Generates a random temperature value between 0 and 30 and if the difference between the current and the average
	 * SensorData value is greater than the alerting Difference, fires off the email functionality.
	 */
	public void generateRandomTemperatureValue() {
		float newTempValue = minTempValue + random.nextFloat() * (maxTempValue - minTempValue);
		logger.info("The newly generated temperature value is: " + newTempValue);
		sensorData.addValue(newTempValue, "Temperature");
		
		if(Math.abs(sensorData.getCurrentValue() - sensorData.getAverageValue()) >= alertingDifference)
			sendNotification(sensorData);
	}

	/**
	 * Schedules the generateRandomTemperatureValue() to run for every 30 seconds.
	 */
	public void scheduleTemperatureMonitoring() {
		eventScheduler = Executors.newSingleThreadScheduledExecutor();
		
		Runnable tempMonitoring = new Runnable() {
			public void run() {
				generateRandomTemperatureValue();
			}
		};
		
		eventScheduler.scheduleWithFixedDelay(tempMonitoring, 1, 30, TimeUnit.SECONDS);
	}
	
	/**
	 * Calls the server setup and email delievry service of the SmtpClientConnector.
	 * 
	 * @param sensorData
	 */
	public void sendNotification(SensorData sensorData) {
		try {
			logger.info("The temperature alert has triggered!!");
			smtpClientConnector.setupEmailSettingsAndSendMail(sensorData);
			this.emailSent = true;
		} catch(Exception e) {
			logger.severe("the email wasn't sent");
		}
	}
	
	/**
	 * Returns the Sensor Data.
	 * 
	 * @return
	 */
	public SensorData getSensorData() {
		return this.sensorData;
	}

	/**
	 * Getter method to check successful email service.
	 * 
	 * @return
	 */
	public boolean isEmailSent() {
		return emailSent;
	}
	
	
}
