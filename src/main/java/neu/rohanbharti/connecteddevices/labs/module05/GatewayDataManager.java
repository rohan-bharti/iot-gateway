package neu.rohanbharti.connecteddevices.labs.module05;

import java.util.logging.Logger;

import org.apache.commons.configuration2.ex.ConfigurationException;

import com.fasterxml.jackson.core.JsonProcessingException;

import neu.rohanbharti.connecteddevices.common.ActuatorData;
import neu.rohanbharti.connecteddevices.common.PersistenceUtil;
import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.common.SensorDataListener;
import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;

public class GatewayDataManager {

	private PersistenceUtil persistenceUtil;
	private SensorDataListener sensorDataListener;
	private ActuatorData actuatorData;
	
	private final float nominalTempValue = 25.0f;
	private final float diffThreashold = 10;
	
	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}
	
	public GatewayDataManager() throws ConfigurationException {
		this.persistenceUtil = new PersistenceUtil();
		this.sensorDataListener = new SensorDataListener();
	}
	
	/**
	 * Starts the Subscription thread and actively listens for SensorData received from the Device app
	 * 
	 * @throws JsonProcessingException
	 * @throws InterruptedException 
	 */
	public void checkForSensorDataUpdate() throws JsonProcessingException, InterruptedException {
		persistenceUtil.registerSensorDataDbmsListener();
		while(true) {
			Thread.sleep(5000);
			if(persistenceUtil.startListeningOnSensorDataChannel(sensorDataListener)) {
				SensorData sensorDataRcvd = sensorDataListener.getSensorData();
				checkForActuation(sensorDataRcvd);
			}
		}
	}
	
	/**
	 * Checks if there is a need for actuation for the received SensorData object
	 * 
	 * @throws JsonProcessingException
	 */
	public void checkForActuation(SensorData sensorData) throws JsonProcessingException {
		//Check for Temperature Actuation
		if (sensorData.getName().equals("Temperature")) {
            float curValue = sensorData.getCurrentValue();
            float diff = curValue - nominalTempValue;
            
            if ( diff > diffThreashold ) {
                logger.info("Actuation in Gateway: Temperature recorded as {TOO HOT}");
                actuatorData = new ActuatorData();
                actuatorData.setCommand("TOO HOT");
                actuatorData.setName("TEMP");
                actuatorData.setValue(curValue);
                persistenceUtil.writeActuatorDataToDbms(actuatorData);
            }    
            else if (diff < (-1 * diffThreashold)) {
                logger.info("Actuation in Gateway: Temperature recorded as {TOO COLD}");
            	actuatorData = new ActuatorData();
            	actuatorData.setCommand("TOO COLD");
            	actuatorData.setValue(curValue);
            	actuatorData.setName("TEMP");
            	persistenceUtil.writeActuatorDataToDbms(actuatorData);
            }
            else {
            	logger.info("Actuation in Gateway: Temperature recorded as {NORMAL}");
            	actuatorData = new ActuatorData();
            	actuatorData.setCommand("NORMAL");
            	actuatorData.setValue(curValue);
            	actuatorData.setName("TEMP");
            	persistenceUtil.writeActuatorDataToDbms(actuatorData);
            }
		}
		
		//Check for Humidity from SenseHat
		else if (sensorData.getName().equals("Humidity_SENSEHAT")) {
			logger.info("Actuation in Gateway: Humidity recorded from SenseHat");
			actuatorData = new ActuatorData();
			actuatorData.setCommand("SENSEHAT NOTIF");
			actuatorData.setValue(sensorData.getCurrentValue());
			actuatorData.setName("Humidity_SENSEHAT");
			persistenceUtil.writeActuatorDataToDbms(actuatorData);
		}

		//Check for Humidity from I2C
		else if (sensorData.getName().equals("Humidity_I2C")) {
			logger.info("Actuation in Gateway: Humidity recorded from I2CBus");
			actuatorData = new ActuatorData();
			actuatorData.setCommand("I2CBUS NOTIF");
			actuatorData.setName("Humidity_I2C");
			actuatorData.setValue(sensorData.getCurrentValue());
			persistenceUtil.writeActuatorDataToDbms(actuatorData);
		}
		
		else {
			logger.info("SensorData not recognized successfully!");
		}	
		
	}
	
	public ActuatorData getActuatorDataAfterActuation() {
		return actuatorData;
	}
	
}
