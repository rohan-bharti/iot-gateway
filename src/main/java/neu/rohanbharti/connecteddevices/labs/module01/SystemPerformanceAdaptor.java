package neu.rohanbharti.connecteddevices.labs.module01;

import java.text.DecimalFormat;
import java.util.logging.Logger;

/**
 * 
 * Class to display the performance information
 * 
 * @author rohan_bharti
 *
 */
public class SystemPerformanceAdaptor {

	private SystemCpuUtilTask systemCpuUtilTask;
	private SystemMemUtilTask systemMemUtilTask;

	private static Logger logger;
	private static DecimalFormat decimalFormat = new DecimalFormat("#.##");

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(SystemPerformanceAdaptor.class.getName());
	}

	/**
	 * Default constructor
	 */
	public SystemPerformanceAdaptor() {
		super();
	}

	/**
	 * Constructor setting SystemCpuUtilTask and SystemMemUtilTask
	 * 
	 * @param systemCpuUtilTask
	 * @param systemMemUtilTask
	 */
	public SystemPerformanceAdaptor(SystemCpuUtilTask systemCpuUtilTask, SystemMemUtilTask systemMemUtilTask) {
		super();
		this.systemCpuUtilTask = systemCpuUtilTask;
		this.systemMemUtilTask = systemMemUtilTask;
	}

	/**
	 * Fetches the cpuUsage and memUsage from both the classes
	 */
	public void displayPerformanceInformation() {

		String cpuUsage = decimalFormat.format(this.systemCpuUtilTask.getDataFromSensor()).toString();
		String memUsage = decimalFormat.format(this.systemMemUtilTask.getDataFromSensor()).toString();

		logger.info("CPU Usage: " + cpuUsage);
		logger.info("Memory Usage: " + memUsage);

	}

}
