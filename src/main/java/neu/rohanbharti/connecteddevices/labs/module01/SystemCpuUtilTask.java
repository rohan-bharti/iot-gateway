package neu.rohanbharti.connecteddevices.labs.module01;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

/**
 * 
 * Class to get the CPU Average Load
 * 
 * @author rohan_bharti
 *
 */
public class SystemCpuUtilTask {

	private Float cpuUsage;
	private OperatingSystemMXBean operatingSystemMXBean;

	public SystemCpuUtilTask() {
		super();
	}

	/**
	 * Instantiates operatingSystemMXBean and fetches the CPU average load
	 * 
	 * @return
	 */
	public Float getDataFromSensor() {
		this.operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		Float cpuUsage = (float) operatingSystemMXBean.getSystemLoadAverage();
		this.setCpuUsage(cpuUsage);
		return getCpuUsage();
	}

	/**
	 * Getter for the cpuUsage
	 * 
	 * @return
	 */
	public Float getCpuUsage() {
		return cpuUsage;
	}

	/**
	 * Setter for the cpuUsage
	 * 
	 * @param cpuUsage
	 */
	public void setCpuUsage(Float cpuUsage) {
		this.cpuUsage = cpuUsage;
	}

}
