package neu.rohanbharti.connecteddevices.labs.module01;

import java.lang.management.MemoryMXBean;
import java.lang.management.ManagementFactory;

/**
 * 
 * Class to get the CPU Memory Usage Load
 * 
 * @author rohan_bharti
 *
 */
public class SystemMemUtilTask {

	private Float memoryUsage;
	private MemoryMXBean memoryMXBean;

	/**
	 * Fetches the memoryUsage using the memoryMXBean bean
	 */
	public Float getDataFromSensor() {
		this.memoryMXBean = ManagementFactory.getMemoryMXBean();
		double usedMemory = (double) memoryMXBean.getHeapMemoryUsage().getUsed();
		double maxMemory = (double) memoryMXBean.getHeapMemoryUsage().getMax();
		double memoryUsage = (usedMemory / maxMemory) * 100d;
		this.setMemoryUsage((float) memoryUsage);
		return getMemoryUsage();
	}

	/**
	 * Getter for memoryUsage
	 * 
	 * @return memoryUsage
	 */
	public Float getMemoryUsage() {
		return memoryUsage;
	}

	/**
	 * Setter for memoryUsage
	 * 
	 * @param memoryUsage
	 */
	public void setMemoryUsage(Float memoryUsage) {
		this.memoryUsage = memoryUsage;
	}

}
