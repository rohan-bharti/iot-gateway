package neu.rohanbharti.connecteddevices.labs.module01;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import neu.rohanbharti.connecteddevices.labs.module02.TempEmulatorAdaptor;

/**
 * 
 * Driver class to display CPU Logistics in a scheduled manner
 * 
 * @author rohan_bharti
 *
 */
public class GatewayHandlerApp {

	private ScheduledExecutorService eventScheduler;
	private SystemPerformanceAdaptor systemPerformanceAdaptor;
	private static int counter = 0;

	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}

	/**
	 * The information is displayed 10 times with a delay for 5 seconds
	 * 
	 * @throws InterruptedException
	 */
	public void displayInformationPeriodically() throws InterruptedException {

		eventScheduler = Executors.newSingleThreadScheduledExecutor();
		systemPerformanceAdaptor = new SystemPerformanceAdaptor(new SystemCpuUtilTask(), new SystemMemUtilTask());

		Runnable display = new Runnable() {
			public void run() {
				counter++;
				systemPerformanceAdaptor.displayPerformanceInformation();
			}
		};

		eventScheduler.scheduleWithFixedDelay(display, 1, 5, TimeUnit.SECONDS);

		while (true) {
			Thread.sleep(2000);
			if (counter == 10) {
				eventScheduler.shutdown();
				logger.info("SCHEDULING IS FINISHED!");
				break;
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {
		GatewayHandlerApp gatewayHandlerApp = new GatewayHandlerApp();
		gatewayHandlerApp.displayInformationPeriodically();
		
		//Starts the temperature values generation and monitoring by sending Emails as configured.
		TempEmulatorAdaptor.demo();
	}
}
