package neu.rohanbharti.connecteddevices.common;

import java.util.logging.Logger;

import org.apache.commons.configuration2.ex.ConfigurationException;

import com.fasterxml.jackson.core.JsonProcessingException;

import neu.rohanbharti.connecteddevices.labs.module01.GatewayHandlerApp;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

public class PersistenceUtil {

	private DataUtil dataUtil;
	private ConfigUtil configUtil;

	private JedisPool jedisPool;
	private JedisPubSub jedisSubsriber;
	private Jedis jedisDatabaseManager;
	private Jedis jedisSubscriberResource;
	private Jedis jedisPublisher;

	private String sensorDataMessage;
	private String actuatorDataMessage;

	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(GatewayHandlerApp.class.getName());
	}

	/**
	 * Constructor
	 * 
	 * @throws ConfigurationException
	 */
	public PersistenceUtil() throws ConfigurationException {
		dataUtil = new DataUtil();
		configUtil = new ConfigUtil();
		setupRedis();
	}

	/**
	 * Subscribes to the sensorDataChannel
	 * 
	 * @param sensorDataListener
	 */
	public void registerSensorDataDbmsListener() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					logger.info("Subscribing to \"sensorDataDeviceChannel\". This thread will be blocked.");
					jedisSubscriberResource.subscribe(jedisSubsriber, "sensorDataDeviceChannel");
					logger.info("Subscription ended.");
				} catch (Exception e) {
					logger.warning("Subscribing failed.");
				}
			}
		}).start();
	}

	/**
	 * Subscribes to the actuatorDataChannel
	 * 
	 * @param sensorDataListener
	 */
	public void registerActuatorDataDbmsListener() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					logger.info("Subscribing to \"actuatorDataDeviceChannel\". This thread will be blocked.");
					jedisSubscriberResource.subscribe(jedisSubsriber, "actuatorDataDeviceChannel");
					logger.info("Subscription ended.");
				} catch (Exception e) {
					logger.warning("Subscribing failed.");
				}
			}
		}).start();
	}

	/**
	 * Writes SensorDataJson to the db, also publishes the new data on its channel
	 * 
	 * @param sensorData
	 */
	public void writeSensorDataToDbms(SensorData sensorData) throws JsonProcessingException {
		try {
			String sensorDataKey = "sensorData" + sensorData.getTimeStamp();
			String sensorDataValue = dataUtil.toJsonFromSensorData(sensorData);

			jedisDatabaseManager.set(sensorDataKey, sensorDataValue);
			jedisPublisher.publish("sensorDataGatewayChannel", sensorDataValue);
			logger.info(
					"SensorData successfully published on sensorDataGatewayChannel and written to the redis database");
		} catch (Exception e) {
			logger.warning("SensorData wasn NOT added to Redis and NOT published on sensorDataGatewayChannel!");
		}
	}

	/**
	 * Writes ActuatorDataJson to the db, also publishes the new data on its channel
	 * 
	 * @param actuatorData
	 */
	public void writeActuatorDataToDbms(ActuatorData actuatorData) throws JsonProcessingException {
		try {
			String actuatorDataKey = "actuatorData";
			String actuatorDataValue = dataUtil.toJsonFromActuatorData(actuatorData);

			jedisDatabaseManager.set(actuatorDataKey, actuatorDataValue);
			jedisPublisher.publish("actuatorDataGatewayChannel", actuatorDataValue);
			logger.info("ActuatorData Value: " + actuatorDataValue);
			logger.info(
					"ActuatorData successfully published on actuatorDataGatewayChannel and written to the redis database!");
		} catch (Exception e) {
			logger.warning("ActuatorData was NOT added to Redis and NOT published on actuatorDataGatewayChannel!");
		}
	}

	/**
	 * Checks if the SensorData has been updated after subscribing to its respective
	 * channel
	 * 
	 */
	public boolean startListeningOnSensorDataChannel(SensorDataListener sensorDataListener) {
		if (getSensorDataMessage() != null && (!getSensorDataMessage().isEmpty())) {
			SensorData sensorData = dataUtil.toSensorDataFromJson(getSensorDataMessage());
			sensorDataListener.onMessage(sensorData);
			setSensorDataMessage(null);
			return true;
		}
		return false;
	}

	/**
	 * Checks if the ActuatorData has been updated after subscribing to its
	 * respective channel
	 * 
	 */
	public boolean startListeningOnActuatorDataChannel(ActuatorDataListener actuatorDataListener) {
		while (true) {
			if (getActuatorDataMessage() != null && (!getActuatorDataMessage().isEmpty())) {
				ActuatorData actuatorData = dataUtil.toActuatorDataFromJson(getActuatorDataMessage());
				actuatorDataListener.onMessage(actuatorData);
				return true;
			}
		}
	}

	/**
	 * Sets up the Redis Pool and its resources for Pub-Sub model
	 * 
	 */
	private void setupRedis() {
		try {
			String host = configUtil.getStringValue("redis.host");
			int port = configUtil.getIntegerValue("redis.port");
			String password = configUtil.getStringValue("redis.password");

			final JedisPoolConfig poolConfig = new JedisPoolConfig();

			jedisPool = new JedisPool(poolConfig, host, port, 0);

			jedisPublisher = jedisPool.getResource();
			jedisPublisher.auth(password);
			jedisSubscriberResource = jedisPool.getResource();
			jedisSubscriberResource.auth(password);
			jedisDatabaseManager = jedisPool.getResource();
			jedisDatabaseManager.auth(password);

			jedisSubsriber = new JedisPubSub() {
				@Override
				public void onMessage(String channel, String message) {
					logger.info("Message received from " + channel + ": " + message);
					if (!channel.equals("sensorDataDeviceChannel"))
						unsubscribe(channel);
					setSensorDataMessage(message);
					super.onMessage(channel, message);
				}

				@Override
				public void onSubscribe(String channel, int subscribedChannels) {
					logger.info("Subscribed to the channel: " + channel);
					super.onSubscribe(channel, subscribedChannels);
				}

				@Override
				public void onUnsubscribe(String channel, int subscribedChannels) {
					logger.info("Unsubscribed from the channel: " + channel);
					super.onUnsubscribe(channel, subscribedChannels);
				}
			};

		} catch (Exception e) {
			logger.warning("The Redis and the Publisher/Subscriber System wasn't set up successfully!");
		}
	}

	public String getSensorDataMessage() {
		return sensorDataMessage;
	}

	public void setSensorDataMessage(String sensorDataMessage) {
		this.sensorDataMessage = sensorDataMessage;
	}

	public String getActuatorDataMessage() {
		return actuatorDataMessage;
	}

	public void setActuatorDataMessage(String actuatorDataMessage) {
		this.actuatorDataMessage = actuatorDataMessage;
	}
}
