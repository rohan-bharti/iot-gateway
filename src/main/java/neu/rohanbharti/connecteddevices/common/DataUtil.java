package neu.rohanbharti.connecteddevices.common;

import java.io.FileWriter;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

/**
 * 
 * Data Util Class
 * 
 * @author rohan_bharti
 *
 */
public class DataUtil {

	public String toJsonFromSensorData(SensorData sensorData) {
	    String sensorDataJsonString = new Gson().toJson(sensorData);
	    return sensorDataJsonString;
	}
	
	public String toJsonFromActuatorData(ActuatorData actuatorData) {
	    String actuatorDataJsonString = new Gson().toJson(actuatorData);
	    return actuatorDataJsonString;
	}
	
	public SensorData toSensorDataFromJson(String sensorDataJsonString) {
		SensorData sensorData = new Gson().fromJson(sensorDataJsonString, SensorData.class);
		return sensorData;
	}
	
	public ActuatorData toActuatorDataFromJson(String actuatorDataString) {
		ActuatorData actuatorData = new Gson().fromJson(actuatorDataString, ActuatorData.class);
		return actuatorData;
	}
	
	public ActuatorData processUbidotsActuatorData(String latestTempActuatorValue) {
		Float value = Float.parseFloat(latestTempActuatorValue);
		ActuatorData actuatorData = new ActuatorData("UBIDOTS_TEMP_ACTUATOR", value, "Temperature");
		return actuatorData;
	}
	
	public String toUbidotJsonFromSensorData(SensorData sensorData) {
		Float tempSensorValue = sensorData.getCurrentValue();
		String tempSensorValueJsonString = "{\"value\":" + tempSensorValue + "}";
		return tempSensorValueJsonString;
	}
	
	public boolean writeSensorDataToFile(SensorData sensorData) {
        try (FileWriter file = new FileWriter("sensorData.txt")) { 
            file.write(toJsonFromSensorData(sensorData));
            file.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
	}
	
	public boolean writeActuatorDataToFile(ActuatorData actuatorData) {
		try (FileWriter file = new FileWriter("actuatorData.json")) { 
            file.write(toJsonFromActuatorData(actuatorData));
            file.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
	}
	
}
