package neu.rohanbharti.connecteddevices.common;

import neu.rohanbharti.connecteddevices.labs.module08.MqttClientConnector;

public class ActuatorDataListener {

	private ActuatorData actuatorData;
	private MqttClientConnector mqttClientConnector = new MqttClientConnector();

	public void onMessage(ActuatorData actuatorData) {
		if (actuatorData.getCommand().equals("UBIDOTS_TEMP_ACTUATOR")) {
			this.actuatorData = actuatorData;
			mqttClientConnector.publishActuatorCommand(actuatorData, 1);
		}
	}

	public ActuatorData getActuatorData() {
		return actuatorData;
	}
}
