package neu.rohanbharti.connecteddevices.common;

import java.sql.Timestamp;    
import java.util.Date;   

/**
 * 
 * Sensor Data Model Class
 * 
 * @author rohan_bharti
 *
 */
public class SensorData {

	private String timeStamp;
	private String name;
	private int sampleCount;
	private float curValue;
	private float totValue;
	private float minValue;
	private float maxValue;
	private float avgValue;
	
	/* Default constructor */
	public SensorData() {
		curValue = 0.0f;
		avgValue = 0.0f;
		minValue = 0.0f;
		maxValue = 0.0f;
		totValue = 0.0f;
		sampleCount = 0;
		timeStamp="";
		name="";
	}
	
	/* adds a value and updates the other related fields as well */
	public void addValue(float val, String name) {
		this.timeStamp = getCurrentTimestamp();
		this.sampleCount += 1;
		this.curValue = val;
		this.totValue += val;
		
		this.name = name;
		
		if (this.sampleCount == 1) {
			this.minValue = this.curValue;
		} else if (this.curValue < this.minValue) {
			this.minValue = this.curValue;
		}
		
		if (this.curValue > this.maxValue) {
			this.maxValue = this.curValue;
		}
		if (this.totValue != 0 && this.sampleCount > 0) {
			this.avgValue = this.totValue / this.sampleCount;
		}
	 }
	
	public String getCurrentTimestamp() {
		Date date = new Date();  
        Timestamp timeStamp = new Timestamp(date.getTime());  
        return timeStamp.toString();
	}
	
	@Override
	public String toString() {
		return "SensorData [timeStamp=" + timeStamp + ", name=" + name + ", curValue=" + curValue + ", avgValue="
				+ avgValue + ", minValue=" + minValue + ", maxValue=" + maxValue + ", totValue=" + totValue
				+ ", sampleCount=" + sampleCount + ", toString()=" + super.toString() + "]";
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getCurrentValue() {
		return curValue;
	}

	public void setCurrentValue(float currentValue) {
		this.curValue = currentValue;
	}

	public float getAverageValue() {
		return avgValue;
	}

	public void setAverageValue(float averageValue) {
		this.avgValue = averageValue;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getTotalValue() {
		return totValue;
	}

	public void setTotalValue(float totalValue) {
		this.totValue = totalValue;
	}

	public int getSampleCount() {
		return sampleCount;
	}

	public void setSampleCount(int sampleCount) {
		this.sampleCount = sampleCount;
	}
	
}
