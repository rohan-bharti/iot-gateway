package neu.rohanbharti.connecteddevices.common;

import java.io.File;
import java.util.logging.Logger;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 * 
 * ConfigUtil class. Sets up the Configuration file and provides API to access data from the configuration file.
 * 
 * @author rohan_bharti
 *
 */
public class ConfigUtil {

	private PropertiesConfiguration config;
	
	private static Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		logger = Logger.getLogger(ConfigUtil.class.getName());
	}

	/* Loads Default Config File */
	public ConfigUtil() throws ConfigurationException {
		loadConfigData(null);
		
	}
	
	/* Loads Config File as per the name supplied */
	public ConfigUtil(String fileName) throws ConfigurationException {
		loadConfigData(fileName);
	}
	
	/* Loads Data from the Config File */
	private void loadConfigData(String fileName) throws ConfigurationException {
		Configurations configs = new Configurations();
		try {
		if(fileName==null || fileName.isEmpty()) {
			this.config = configs.properties(new File("ConnectedDevicesConfig.props"));
			logger.info("The Props File was Loaded Successfully from the default file");
		} else {
			this.config = configs.properties(new File(fileName));
			logger.info("The Props File was Loaded Successfully");
		}
		} catch (Exception e) {
			logger.warning("The props file wasn't loaded successfully");
		}
	}
	
	/* Fetches String Value from the ConfigFile as per the key supplied */
	public String getStringValue(String key) {
		return this.config.getString(key);
	}
	
	/* Fetches Integer Value from the ConfigFile as per the key supplied */
	public int getIntegerValue(String key) {
		return this.config.getInt(key);
	}
	
	/* Fetches Boolean Value from the ConfigFile as per the key supplied */
	public boolean getBooleanValue(String key) {
		return this.config.getBoolean(key);
	}
	
	/* Fetches Value from the ConfigFile if the key exists */
	public boolean hasProperty(String key) {
		return this.config.containsKey(key);
	}
	
	/* Checks if data has been loaded from the config file successfully */
	public boolean hasDataLoadedSuccessfully() {
		if(this.config.getKeys().hasNext())
			return true;
		else
			return false;
	}
	
	/* Checks if the config object loaded is empty or not */
	public boolean hasConfigData() {
		if(this.config.isEmpty())
			return false;
		else
			return true;
	}
}
