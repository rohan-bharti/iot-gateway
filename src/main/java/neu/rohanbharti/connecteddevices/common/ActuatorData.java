package neu.rohanbharti.connecteddevices.common;

public class ActuatorData {

	private String command;
	private float value;
	private String name;
	
	public ActuatorData() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ActuatorData(String command, float value, String name) {
		super();
		this.command = command;
		this.value = value;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "ActuatorData [command=" + command + ", value=" + value + ", name=" + name + "]";
	}
	
	public String getCommand() {
		return command;
	}
	
	public void setCommand(String command) {
		this.command = command;
	}
	
	public float getValue() {
		return value;
	}
	
	public void setValue(float value) {
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
