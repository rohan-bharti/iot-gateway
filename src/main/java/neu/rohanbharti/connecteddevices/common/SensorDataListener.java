package neu.rohanbharti.connecteddevices.common;

public class SensorDataListener {

	private SensorData sensorData;
	
	public void onMessage(SensorData sensorData) {
		this.sensorData = sensorData;
	}
	
	public SensorData getSensorData() {
		return sensorData;
	}
	
}
