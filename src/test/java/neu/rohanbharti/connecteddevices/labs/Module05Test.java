package neu.rohanbharti.connecteddevices.labs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import neu.rohanbharti.connecteddevices.common.ActuatorData;
import neu.rohanbharti.connecteddevices.common.DataUtil;
import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.labs.module05.GatewayDataManager;

/**
 * Test class for all requisite Module05 functionality.
 */
public class Module05Test
{
	private DataUtil dataUtil;
	private GatewayDataManager gatewayDataManager;
	private ActuatorData actuatorData;
	private SensorData sensorData;
	
	/**
	 * Set Up Method
	 */
	@Before
	public void setUp() throws Exception
	{
		dataUtil = new DataUtil();
		gatewayDataManager = new GatewayDataManager();
	}
	
	/**
	 * Test actuation for Hot Temperature
	 * 
	 * @throws JsonProcessingException 
	 */
	@Test
	public void testTemperatureHotActuation() throws JsonProcessingException
	{
		sensorData = new SensorData();
		sensorData.addValue(40, "Temperature");
		gatewayDataManager.checkForActuation(sensorData);
		actuatorData = gatewayDataManager.getActuatorDataAfterActuation();
		Assert.assertNotNull(actuatorData);
		Assert.assertEquals("TOO HOT", actuatorData.getCommand());
	}
	
	/**
	 * Test actuation for Cold Temperature
	 * 
	 * @throws JsonProcessingException 
	 */
	@Test
	public void testTemperatureColdActuation() throws JsonProcessingException
	{
		sensorData = new SensorData();
		sensorData.addValue(5, "Temperature");
		gatewayDataManager.checkForActuation(sensorData);
		actuatorData = gatewayDataManager.getActuatorDataAfterActuation();
		Assert.assertNotNull(actuatorData);
		Assert.assertEquals("TOO COLD", actuatorData.getCommand());
	}
	
	/**
	 * Test actuation for Normal Temperature
	 * 
	 * @throws JsonProcessingException 
	 */
	@Test
	public void testTemperatureNormalActuation() throws JsonProcessingException
	{
		sensorData = new SensorData();
		sensorData.addValue(20, "Temperature");
		gatewayDataManager.checkForActuation(sensorData);
		actuatorData = gatewayDataManager.getActuatorDataAfterActuation();
		Assert.assertNotNull(actuatorData);
		Assert.assertEquals("NORMAL", actuatorData.getCommand());
	}
	
	/**
	 * Test actuation for Senshat Humidity
	 * 
	 * @throws JsonProcessingException 
	 */
	@Test
	public void testHumiditySensehatActuation() throws JsonProcessingException
	{
		sensorData = new SensorData();
		sensorData.addValue(11, "Humidity_SENSEHAT");
		gatewayDataManager.checkForActuation(sensorData);
		actuatorData = gatewayDataManager.getActuatorDataAfterActuation();
		Assert.assertNotNull(actuatorData);
		Assert.assertEquals("SENSEHAT NOTIF", actuatorData.getCommand());
	}
	
	/**
	 * Test actuation for I2C Humidity
	 * 
	 * @throws JsonProcessingException 
	 */
	@Test
	public void testHumidityI2CActuation() throws JsonProcessingException
	{
		sensorData = new SensorData();
		sensorData.addValue(11, "Humidity_I2C");
		gatewayDataManager.checkForActuation(sensorData);
		actuatorData = gatewayDataManager.getActuatorDataAfterActuation();
		Assert.assertNotNull(actuatorData);
		Assert.assertEquals("I2CBUS NOTIF", actuatorData.getCommand());
	}
	
}
