package neu.rohanbharti.connecteddevices.labs;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.labs.module02.TempSensorEmulator;

/**
 * 
 * Test class for all the classes under the module 2 Package.
 * 
 * @author rohan_bharti
 *
 */
public class Module02Test
{
	private TempSensorEmulator tempSensorEmulator;
	
	/**
	 * Sets up the objects required for testing
	 */
	@Before
	public void setUp() throws Exception
	{
		this.tempSensorEmulator = new TempSensorEmulator();
	}
	
	/**
	 * Tests the random generation of the Temperature Values.
	 */
	@Test
	public void testRandomValuesGeneration()
	{
		tempSensorEmulator.generateRandomTemperatureValue();
		SensorData sensorData = tempSensorEmulator.getSensorData();
		assertTrue("Sample Count is 1", sensorData.getSampleCount() == 1);
		assertTrue("Value is between 0 and 30", sensorData.getCurrentValue() <= 30 && sensorData.getCurrentValue() >= 0);
	}
	
	/**
	 * [INTEGRATION TEST]
	 * Tests the actual email Service. The test should pass if no exception is thrown.
	 * This test also checks all the functions of SmtpClientConnector class as well.
	 * 
	 * Has been commented out because it uses the actual ConnectedDevicesConfig.props file 
	 * which contains sensitive information. Only for Local testing.
	 */
//	@Test
//	public void testEmailService() {
//		tempSensorEmulator.generateRandomTemperatureValue();
//		SensorData sensorData = tempSensorEmulator.getSensorData();
//		tempSensorEmulator.sendNotification(sensorData);
//		assertTrue("Email was sent successfully", tempSensorEmulator.isEmailSent());
//	}
	
}
