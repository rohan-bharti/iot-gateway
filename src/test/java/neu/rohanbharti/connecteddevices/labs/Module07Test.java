package neu.rohanbharti.connecteddevices.labs;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.labs.module07.CoapServerManager;
import neu.rohanbharti.connecteddevices.labs.module07.TemperatureSensorDataHandler;

/**
 * Test class for all requisite Module07 functionality.
 */
public class Module07Test
{
	private TemperatureSensorDataHandler temperatureSensorDataHandler;
	private CoapServerManager coapServerManager;
	
	private SensorData sensorData;

	/**
	 * Set Up Method
	 */
	@Before
	public void setUp() throws Exception {
		coapServerManager = new CoapServerManager();
		temperatureSensorDataHandler = new TemperatureSensorDataHandler("/tempSensorData");
		
		this.sensorData = new SensorData();
		sensorData.addValue(20, "Temperature");
		temperatureSensorDataHandler.getSensorDataListener().onMessage(sensorData);
	}
	
	/**
	 * Test the startup of the Server
	 */
	@Test
	public void testCoapServerStartup()
	{
		assertTrue(coapServerManager.startSever());
	}
	
	/**
	 * Test stopping of the Server
	 */
	@Test
	public void testCoapServerStop()
	{
		assertTrue(coapServerManager.stopServer());
	}
	
}
