package neu.rohanbharti.connecteddevices.labs;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import neu.rohanbharti.connecteddevices.labs.module01.SystemCpuUtilTask;
import neu.rohanbharti.connecteddevices.labs.module01.SystemMemUtilTask;

/**
 * Test class for all requisite Module01 functionality.
 */
public class Module01Test {
	private SystemCpuUtilTask systemCpuUtilTask;
	private SystemMemUtilTask systemMemUtilTask;

	/**
	 * Initializes the objects for all the classes relevant
	 * 
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		systemCpuUtilTask = new SystemCpuUtilTask();
		systemMemUtilTask = new SystemMemUtilTask();
	}

	/**
	 * Checks if the cpuUsage value is between 0.0 and 100.0
	 */
	@Test
	public void testCpuUsageFetch() {
		systemCpuUtilTask.getDataFromSensor();
		Float cpuUsage = systemCpuUtilTask.getCpuUsage();
		if (!(cpuUsage > 0.0 && cpuUsage < 100.0))
			fail("The value isn't between 0.0 and 100");
	}

	/**
	 * Checks if the cpuMem value is between 0.0 and 100.0
	 */
	@Test
	public void testMemUsageFetch() {
		systemMemUtilTask.getDataFromSensor();
		Float memUsage = systemMemUtilTask.getMemoryUsage();
		if (!(memUsage > 0.0 && memUsage < 100.0))
			fail("The value isn't between 0.0 and 100");
	}

}
