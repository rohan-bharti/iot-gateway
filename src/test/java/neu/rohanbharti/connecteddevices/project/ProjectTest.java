/**
 * 
 */
package neu.rohanbharti.connecteddevices.project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import neu.rohanbharti.connecteddevices.common.ActuatorData;
import neu.rohanbharti.connecteddevices.common.SensorData;
import neu.rohanbharti.connecteddevices.labs.module07.CoapServerManager;
import neu.rohanbharti.connecteddevices.labs.module07.TemperatureSensorDataHandler;
import neu.rohanbharti.connecteddevices.labs.module08.MqttClientConnector;

/**
 * Test class for all requisite Project functionality.
 */
public class ProjectTest
{
	private SensorData sensorData;
	private ActuatorData actuatorData;
	private TemperatureSensorDataHandler temperatureSensorDataHandler;
	private CoapServerManager coapServerManager;

	/**
	 * Set Up Method
	 */
	@Before
	public void setUp() throws Exception {
		this.sensorData = new SensorData();
		sensorData.addValue(20, "Temperature");

		this.actuatorData = new ActuatorData();
		actuatorData.setCommand("TOO HOT");
		actuatorData.setName("TEMP");
		actuatorData.setValue(40);
		
		coapServerManager = new CoapServerManager();
		temperatureSensorDataHandler = new TemperatureSensorDataHandler("/tempSensorData");
	}
	
	/**
	 * Tests publishing the SensorData and ActuatorData on the test Channel. Commented because
	 * running on Bitbucket breaks the pipeline. Needs to run on the local.
	 */
	@Test
	public void testPublishingSensorDataAndActuatorData() {
		boolean actuationPublishStatus = new MqttClientConnector().publishSensorData(sensorData, 2);
		Assert.assertTrue(actuationPublishStatus);
		boolean sensorPublishStatus = new MqttClientConnector().publishActuatorCommand(actuatorData, 2);
		Assert.assertTrue(sensorPublishStatus);
	}

	/**
	 * Tests receiving and processing of SensorDataJson on the test Channel by
	 * another MQTT Client
	 */
	@Test
	public void testSensorDataSubscription() {
		String sensorDataJson = "{\"timeStamp\": \"2020-02-20 00:26:41.758011\", \"sampleCount\": 1, \"curValue\": 20, \"totValue\": 20, \"minValue\": 20, \"maxValue\": 20, \"avgValue\": 20.0, \"name\": \"Temperature\"}";
		byte[] payload = sensorDataJson.getBytes();
		MqttMessage message = new MqttMessage(payload);
		SensorData sensorData = new MqttClientConnector().processSensorData(message);
		assertEquals(1, sensorData.getSampleCount());
		assertTrue(20.0 == sensorData.getMaxValue());
		assertTrue(20.0 == sensorData.getMinValue());
		assertTrue(20.0 == sensorData.getTotalValue());
		assertTrue(20.0 == sensorData.getAverageValue());
		String timeStamp = sensorData.getCurrentTimestamp();
		assertTrue(sensorData.getTimeStamp().equals("2020-02-20 00:26:41.758011"));
		assertEquals("Temperature", sensorData.getName());
	}
	
	/**
	 * Test the startup of the Server
	 */
	@Test
	public void testCoapServerStartup()
	{
		assertTrue(coapServerManager.startSever());
	}
	
	/**
	 * Test stopping of the Server
	 */
	@Test
	public void testCoapServerStop()
	{
		assertTrue(coapServerManager.stopServer());
	}

	
}
