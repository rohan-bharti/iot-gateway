package neu.rohanbharti.connecteddevices.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * Test class for Sensor Data.
 * 
 * @author rohan_bharti
 *
 */
public class SensorDataTest {
	
	private SensorData sensorData;
	
	/**
	 * Instantiates the SensorData class and creates dummy data for testing
	 */
	@Before
	public void setUp() throws Exception
	{
		this.sensorData = new SensorData();
		sensorData.addValue(2.0f, "temperature");
		sensorData.addValue(4.0f, "temperature");
		sensorData.addValue(6.0f, "temperature");
		sensorData.addValue(8.0f, "temperature");
		sensorData.addValue(10.0f, "temperature");
	}
	
	/**
	 * Tests if all the related fields of the sensorData have been updated properly 
	 */
	@Test
	public void testUpdatedFields()
	{
		assertTrue("Current Value updated", sensorData.getCurrentValue() == 10.0f);
		assertTrue("Average Value updated", sensorData.getAverageValue() == 6.0f);
		assertTrue("Current Value updated", sensorData.getMinValue() == 2.0f);
		assertTrue("Current Value updated", sensorData.getMaxValue() == 10.0f);
		assertTrue("Current Value updated", sensorData.getTotalValue() == 30.0f);
	}
	
}
