package neu.rohanbharti.connecteddevices.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Test class for DataUtil functionality.
 *
 */
public class DataUtilTest {

	private final DataUtil dataUtil = new DataUtil();
	private ActuatorData actuatorData;
	private SensorData sensorData;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		sensorData = new SensorData();
		sensorData.addValue(20, "Temperature");
		actuatorData = new ActuatorData();
		actuatorData.setCommand("HOT TEMP");
		actuatorData.setName("Temp");
		actuatorData.setValue(40);
	}

	/**
	 * Checks for conversion of ActuatorData to Json
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void testActuatorDataToJson() throws JsonProcessingException {
		String expectedJson = "{\"command\":\"HOT TEMP\",\"value\":40.0,\"name\":\"Temp\"}";
		String actualJson = dataUtil.toJsonFromActuatorData(actuatorData);
		assertEquals(expectedJson, actualJson);
	}

	/**
	 * Tests conversion of SensorData to Json
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	public void testSensorDataToJson() throws JsonProcessingException {
		String actualJson = dataUtil.toJsonFromSensorData(sensorData);
		assertNotNull(actualJson);
	}

	/**
	 * Tests Json to ActuatorData
	 */
	@Test
	public void testJsonToActuatorData() {
		String actuatorJson = "{\"command\":\"HOT TEMP\",\"value\":40.0,\"name\":\"Temp\"}";
		ActuatorData actuatorData = dataUtil.toActuatorDataFromJson(actuatorJson);
		assertEquals("HOT TEMP", actuatorData.getCommand());
		assertEquals("Temp", actuatorData.getName());
		assertTrue(actuatorData.getValue() == 40.0);
	}

	/**
	 * Tests Json to SensorData
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testJsonToSensorData() {
		String sensorDataJson = "{\"timeStamp\": \"2020-02-20 00:26:41.758011\", \"sampleCount\": 1, \"curValue\": 20, \"totValue\": 20, \"minValue\": 20, \"maxValue\": 20, \"avgValue\": 20.0, \"name\": \"Temperature\"}";
		SensorData sensorData = dataUtil.toSensorDataFromJson(sensorDataJson);
		assertEquals(1, sensorData.getSampleCount());
		assertTrue(20.0 == sensorData.getMaxValue());
		assertTrue(20.0 == sensorData.getMinValue());
		assertTrue(20.0 == sensorData.getTotalValue());
		assertTrue(20.0 == sensorData.getAverageValue());
		String timeStamp = sensorData.getCurrentTimestamp();
		assertTrue(sensorData.getTimeStamp().equals("2020-02-20 00:26:41.758011"));
		assertEquals("Temperature", sensorData.getName());
	}

}
