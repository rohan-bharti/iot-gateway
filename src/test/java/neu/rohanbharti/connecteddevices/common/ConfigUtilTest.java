package neu.rohanbharti.connecteddevices.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * Test class for ConfigUtil.
 * 
 * @author rohan_bharti
 *
 */
public class ConfigUtilTest
{
	
	private final String CONFIG_PROPS_TEST_FILE = "TestConfig.props";
	private ConfigUtil configUtil;
	
	/**
	 * Sets up the test configuration file
	 */
	@Before
	public void setUp() throws Exception
	{
		configUtil = new ConfigUtil(CONFIG_PROPS_TEST_FILE);
	}
	
	
	/**
	 * Test method for checking if we can successfully fetch a Boolean Value
	 */
	@Test
	public void testGetBooleanProperty()
	{
		assertTrue(configUtil.getBooleanValue("handsome"));
	}
	
	/**
	 * Test method for checking if we can successfully fetch an int Value
	 */
	@Test
	public void testGetIntegerProperty()
	{
		int expected = 23;
		int actual = configUtil.getIntegerValue("age");
		assertTrue("The two ints are equal", expected == actual);
	}
	
	/**
	 * Test method for checking if we can successfully fetch a String Value
	 */
	@Test
	public void testGetProperty()
	{
		String expected = "Rohan";
		String actual = configUtil.getStringValue("firstname");
		assertTrue("The two Strings are equal", expected.equals(actual));
	}
	
	/**
	 * Test method for checking if a key exists in the Config.Props file
	 */
	@Test
	public void testHasProperty()
	{
		assertTrue("The key supplied exists", configUtil.hasProperty("lastname"));
	}
	
	/**
	 * Test method for checking if a section exists in the Config.Props file. However in Java, when we read teh Config
	 * file, it doesn't put keys in separate sections. All sections are also read as keys. See the documentation of 
	 * Apache Commons Configuration2.
	 */
	@Test
	public void testHasSection()
	{
		assertTrue("The section exists", configUtil.hasProperty("[FILLEDSECTION]"));
	}
	
	/**
	 * Test method to check if the data is successfully loaded.
	 */
	@Test
	public void testIsConfigDataLoaded()
	{
		assertTrue(configUtil.hasDataLoadedSuccessfully());
	}
	
}
